use std::any::TypeId;
use std::collections::HashMap;
use std::hash::{Hash, Hasher};
use std::mem;
use crate::allocators::StringInternTable;
use crate::bytecode::Operation;
use crate::grammar::{Literal, Number};

// IDs are glorified pointers
pub type AnyID = u64;
pub type TableID = AnyID;
pub type RecordID = AnyID;
pub type StringID = AnyID;

// A primitive is anything that can be stored in
// a register directly, i.e., something that can
// be represented as a u64
#[derive(Copy, Clone)]
pub enum Primitive {
    Bool(bool),
    Integer(i64),
    Float(FloatD),
    Any(AnyID),
    TableId(TableID),
    RecordId(RecordID),
    String(StringID),
    Function(ProgramIndex),
    Nil,
}

#[derive(Copy, Clone, PartialEq, Eq, Hash)]
pub enum TableKey {
    Bool(bool),
    Integer(i64),
    String(StringID),
}

impl TableKey {
    pub fn from_primitive(primitive: &Primitive) -> Self {
        match primitive {
            Primitive::Bool(b) => TableKey::Bool(*b),
            Primitive::Integer(i) => TableKey::Integer(*i),
            Primitive::String(s) => TableKey::String(*s),
            _ => panic!("invalid primitive as table key")
        }
    }
}

pub struct Table {
    pub id: TableID,
    pub data: HashMap<TableKey, Primitive>,
}

pub struct Record {
    pub name: String,
    pub field_names: HashMap<StringID, usize>,
    pub field_types: Vec<TypeId>,
    pub fields: Vec<Primitive>,
}

impl Record {
    pub fn add_field(&mut self, name: String, field_type: TypeId) {
        self.field_types.push(field_type);
        self.field_names.insert(name.to_string(), self.fields.len() - 1);
    }

    pub fn get_field(&self, primitive: &Primitive) -> Primitive {
        match primitive {
            Primitive::Integer(i) => self.fields.get(*i as usize).unwrap().clone(),
            Primitive::String(s) => self.fields.get(*self.field_names.get(s).unwrap()).unwrap().clone(),
            _ => panic!("invalid record field access type")
        }
    }
}

// An index into a program's list of operations,
// effectively an address
pub type ProgramIndex = usize;

pub struct Program {
    program: Vec<Operation>,
    functions: HashMap<String, ProgramIndex>,
}

impl Program {
    pub fn is_function(&self, function_name: &str) -> bool {
        self.functions.contains_key(function_name)
    }
    
    pub fn get_function(&self, function_name: &str) -> ProgramIndex {
        *self.functions.get(function_name).unwrap()
    }

    pub fn push(&mut self, operation: Operation) {
        self.program.push(operation)
    }

    pub fn append(&mut self, other: &mut Vec<Operation>) {
        self.program.append(other)
    }
}

pub type RegisterIndex = usize;

pub enum Stored {
    Table(Table),
    Record(Record),
}

pub struct Runtime {
    // Registers
    pub registers: Vec<u64>,

    // Memory
    store: HashMap<AnyID, Stored>,
    pub strings: StringInternTable,
}

impl Runtime {
    fn run(&mut self, program: &Program) {

    }

    pub fn get(&self, id: AnyID) -> &Stored {
        self.store.get(&id).unwrap()
    }
}
