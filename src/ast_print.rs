use crate::visit::{Parsed, Visitor};
use crate::grammar::Assignment;
use crate::grammar::Block;
use crate::grammar::Callsite;
use crate::grammar::Class;
use crate::grammar::Expression;
use crate::grammar::For;
use crate::grammar::Function;
use crate::grammar::Identifier;
use crate::grammar::If;
use crate::grammar::Operation;
use crate::grammar::Print;
use crate::grammar::Program;
use crate::grammar::Return;
use crate::grammar::Variable;
use crate::grammar::While;

pub struct AstPrinter {
    pub printable: String,
}

impl Visitor for AstPrinter {
    fn visit_program(&mut self, program: &Program) {
        for decl in program {
            decl.accept(self)
        }
    }

    fn visit_class(&mut self, class: &Class) {
        self.printable += &*format!("class {} {{\n", class.name);
        let proc_method = |method: &Function| {
            self.printable += "\t";
            method.accept(self);
        };
        class.methods.iter().for_each(proc_method);
        self.printable += "}\n";
    }

    fn visit_function(&mut self, function: &Function) {
        self.printable += &*format!("fun {}(", function.name);
        function.parameters.iter().for_each(|param| self.printable += &*(param.to_string() + ", "));
        self.printable += ") {{\n";
        function.body.accept(self);
        self.printable += "}}\n";
    }

    fn visit_variable(&mut self, variable: &Variable) {
        self.printable += &*format!("var {} = ", variable.identifier);
        variable.expression.accept(self);
    }

    fn visit_assignment(&mut self, statement: &Assignment) {
        statement.assigned.accept(self);
        self.printable += " = ";
        statement.expression.accept(self);
    }

    fn visit_for(&mut self, statement: &For) {
        self.printable += "for (";
        if statement.init_variable.is_some() {
            statement.init_variable.as_ref().unwrap().accept(self);
        }
        self.printable += "; ";
        if statement.condition.is_some() {
            statement.condition.as_ref().unwrap().accept(self);
        }
        self.printable += "; ";
        if statement.iter_variable.is_some() {
            statement.iter_variable.as_ref().unwrap().accept(self);
        }
        self.printable += ") {\n";
        statement.body.accept(self);
        self.printable += "}\n";
    }

    fn visit_if(&mut self, statement: &If) {
        self.printable += "if (";
        statement.condition.accept(self);
        self.printable += ") {\n";
        statement.body.accept(self);
        if statement.else_block.is_some() {
            self.printable += "} else {\n";
            statement.else_block.as_ref().unwrap().accept(self);
        }
        self.printable += "}\n";
    }

    fn visit_print(&mut self, expression: &Print) {
        self.printable += "print ";
        expression.accept(self);
    }

    fn visit_return(&mut self, expression: &Return) {
        self.printable += "return";
        if expression.is_some() {
            self.printable += " ";
            expression.as_ref().unwrap().accept(self);
        }
    }

    fn visit_while(&mut self, statement: &While) {
        self.printable += "while (";
        statement.condition.accept(self);
        self.printable += ") {\n";
        statement.body.accept(self);
        self.printable += "}\n";
    }

    fn visit_block(&mut self, statement: &Block) {
        self.printable += "{\n";
        for stmt in statement {
            stmt.accept(self);
        }
        self.printable += "}\n";
    }

    fn visit_callsite(&mut self, statement: &Callsite) {
        statement.function.accept(self);
        self.printable += "(";

        let proc_arg = |arg: &Expression| {
            arg.accept(self);
            self.printable += ", ";
        };
        statement.arguments.iter().for_each(proc_arg);
        self.printable += ")";
    }

    fn visit_integer(&mut self, literal: i64) {
        self.printable += &*literal.to_string();
    }

    fn visit_float(&mut self, literal: f64) {
        self.printable += &*literal.to_string();
    }

    fn visit_boolean(&mut self, literal: bool) {
        self.printable += &*literal.to_string();
    }

    fn visit_nil(&mut self) {
        self.printable += "nil";
    }

    fn visit_string(&mut self, literal: &String) {
        self.printable += &*literal;
    }

    fn visit_this(&mut self) {
        self.printable += "this";
    }

    fn visit_identifier(&mut self, identifier: &Identifier) {
        self.printable += identifier;
    }

    fn visit_member(&mut self, object: &Box<Expression>, member: &Identifier) {
        object.accept(self);
        self.printable += &*format!(".{}", member);
    }

    fn visit_unary(&mut self, op: &Operation, exp: &Box<Expression>) {
        self.printable += match op {
            Operation::Minus => { "-" }
            Operation::Not => { "!" }
            _ => { panic!("{} is not a valid unary operator", stringify!(op)) }
        };
        self.printable += " ";
        exp.accept(self);
    }

    fn visit_binary(&mut self, lexp: &Box<Expression>, op: &Operation, rexp: &Box<Expression>) {
        lexp.accept(self);
        self.printable += match op {
            Operation::LogicOr => { "or" }
            Operation::LogicAnd => { "and" }
            Operation::NotEqual => { "!=" }
            Operation::Equal => { "==" }
            Operation::GreaterThan => { ">" }
            Operation::GreaterEqual => { ">=" }
            Operation::LessThan => { "<" }
            Operation::LessEqual => { "<=" }
            Operation::Plus => { "+" }
            Operation::Minus => { "-" }
            Operation::Divide => { "/" }
            Operation::Multiply => { "*" }
            Operation::Not => { "!" }
            _ => { panic!("{} is not a binary operator", stringify!(op)) }
        };
        rexp.accept(self);
    }

    fn visit_call_arguments(&mut self, call_arguments: &Vec<Expression>) {
        self.printable += "(";
        for arg in call_arguments {
            arg.accept(self);
            self.printable += ", ";
        }
        self.printable += ")";
    }

    fn visit_group(&mut self, group: &Box<Expression>) {
        self.printable += "(";
        group.accept(self);
        self.printable += ")";
    }
}
