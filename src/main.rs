mod grammar;
mod visit;
mod ast_print;
mod runtime;
mod bytecode_generator;
mod bytecode;
mod allocators;
mod encode;

use std::{env, fs};
use crate::ast_print::AstPrinter;
use crate::visit::Parsed;
use crate::grammar::{program};

// #[derive(Copy, Clone)]
// struct SourceLocation {
//     line_number: usize,
//     column_index: usize,
// }
//
// struct Error {
//     location: SourceLocation,
//     message: String,
// }

#[cfg(test)]
mod tests {
    use crate::grammar::{block, boolean, Class, Declaration, Expression, expression, function, Function, If, if_statement, Literal, number, Number, Operation, print_statement, Statement, string, Variable, variable_declaration, class_declaration, program, for_statement, For, Assignment, Assignable};

    #[test]
    fn literals() {
        assert_eq!(number("3"), Ok(("", Number::Integer(3))));
        assert_eq!(number("3.15"), Ok(("", Number::Float(3.15))));

        assert_eq!(string("\"hello\""), Ok(("", "hello".to_string())));
        assert_eq!(string("\"h))%$@&*%$`~12&@*(&$@(*&(*%@(*%);;;;1283//\\\""), Ok(("", "h))%$@&*%$`~12&@*(&$@(*&(*%@(*%);;;;1283//\\".to_string())));

        assert_eq!(boolean("true"), Ok(("", Literal::Boolean(true))));
        assert_eq!(boolean("false"), Ok(("", Literal::Boolean(false))));
    }

    #[test]
    fn expressions() {
        assert_eq!(expression("snake_case"), Ok(("", Expression::Identifier("snake_case".to_string()))));

        assert_eq!(expression("super.aaaa"), Ok(("", Expression::Member(Box::new(Expression::Identifier("super".to_string())), "aaaa".to_string()))));

        assert_eq!(expression("-1"), Ok(("", Expression::Literal(Literal::Number(Number::Integer(-1))))));

        assert_eq!(expression("-x"), Ok(("", Expression::Unary(
            Operation::Minus,
            Box::new(Expression::Identifier("x".to_string())),
        ))));

        assert_eq!(expression("true != false"), Ok(("", Expression::Binary(
            Box::new(Expression::Literal(Literal::Boolean(true))),
            Operation::NotEqual,
            Box::new(Expression::Literal(Literal::Boolean(false))),
        ))));

        assert_eq!(expression("true == false == false"), Ok(("", Expression::Binary(
            Box::new(Expression::Literal(Literal::Boolean(true))),
            Operation::Equal,
            Box::new(Expression::Binary(
                Box::new(Expression::Literal(Literal::Boolean(false))),
                Operation::Equal,
                Box::new(Expression::Literal(Literal::Boolean(false))),
            )),
        ))));

        assert_eq!(expression("a.x"), Ok(("", Expression::Member(
            Box::new(Expression::Identifier("a".to_string())), "x".to_string()
        ))));

        assert_eq!(expression("a.x == b.x"), Ok(("", Expression::Binary(
            Box::new(Expression::Member(
                Box::new(Expression::Identifier("a".to_string())), "x".to_string()
            )),
            Operation::Equal,
            Box::new(Expression::Member(
                Box::new(Expression::Identifier("b".to_string())), "x".to_string()
            ))
        ))));
    }

    #[test]
    fn blocks() {
        assert_eq!(block("{}"), Ok(("", vec![])));

        assert_eq!(block("{\n\tvar nil_thing = nil\n}"), Ok(("", vec![Declaration::Variable(
            Variable {
                identifier: "nil_thing".to_string(),
                expression: Expression::Literal(Literal::Nil)
            }
        )])));

        assert_eq!(block("{\n\tprint \"hello pi\"\n}"), Ok(("",  vec![
            Declaration::Statement(Statement::Print(Expression::Literal(Literal::String("hello pi".to_string()))))
        ])));

        assert_eq!(block("{\n\tvar a = nil\n\tvar b = a\n}"), Ok(("", vec![
            Declaration::Variable(Variable { identifier: "a".to_string(), expression: Expression::Literal(Literal::Nil) }),
            Declaration::Variable(Variable { identifier: "b".to_string(), expression: Expression::Identifier("a".to_string()) }),
        ])));
    }

    #[test]
    fn statements() {
        assert_eq!(print_statement("print \"hello world\"\n"), Ok(("\n", Expression::Literal(
            Literal::String("hello world".to_string()))
        )));

        assert_eq!(if_statement("if (a == 3.14) {\n\tprint \"hello pi\"\n}"), Ok(("", If {
            condition: Expression::Binary(
                Box::new(Expression::Identifier("a".to_string())),
                Operation::Equal,
                Box::new(Expression::Literal(Literal::Number(Number::Float(3.14)))),
            ),
            body: vec![
                Declaration::Statement(Statement::Print(Expression::Literal(Literal::String("hello pi".to_string()))))
            ],
            else_block: None,
        })));

        assert_eq!(if_statement("if (a == b) {\n\tvar d = d\n} else {\n\tvar e = e\n}"), Ok(("", If {
            condition: Expression::Binary(Box::new(Expression::Identifier("a".to_string())), Operation::Equal, Box::new(Expression::Identifier("b".to_string()))),
            body: vec![
                Declaration::Variable(Variable { identifier: "d".to_string(), expression: Expression::Identifier("d".to_string()) })
            ],
            else_block: Some(vec![
                Declaration::Variable(Variable { identifier: "e".to_string(), expression: Expression::Identifier("e".to_string()) })
            ]),
        })));

        assert_eq!(for_statement("for (;;) {\n\tprint \"loop\"\n}"), Ok(("", For {
            init_variable: None,
            condition: None,
            iter_variable: None,
            body: vec![
                Declaration::Statement(Statement::Print(Expression::Literal(Literal::String("loop".to_string()))))
            ],
        })));

        assert_eq!(for_statement("for (var i = 0; i < 10; i = i + 1) {\n\tprint \"loop!\"\n}"), Ok(("", For {
            init_variable: Some(Variable { identifier: "i".to_string(), expression: Expression::Literal(Literal::Number(Number::Integer(0))) }),
            condition: Some(Expression::Binary(Box::new(Expression::Identifier("i".to_string())), Operation::LessThan, Box::new(Expression::Literal(Literal::Number(Number::Integer(10)))))),
            iter_variable: Some(Box::new(Statement::Assignment(Assignment { assigned: Assignable::Identifier("i".to_string()), expression: Expression::Binary(Box::new(Expression::Identifier("i".to_string())), Operation::Plus, Box::new(Expression::Literal(Literal::Number(Number::Integer(1))))) }))),
            body: vec![
                Declaration::Statement(Statement::Print(Expression::Literal(Literal::String("loop!".to_string()))))
            ],
        })));
    }

    #[test]
    fn function_declaration() {
        assert_eq!(function("fun add(x, y) {\n\treturn x + y\n}"), Ok(("", Function {
            name: "add".to_string(),
            parameters: vec![
                "x".to_string(),
                "y".to_string(),
            ],
            body: vec![
                Declaration::Statement(Statement::Return(Some(Expression::Binary(
                    Box::new(Expression::Identifier("x".to_string())),
                    Operation::Plus,
                    Box::new(Expression::Identifier("y".to_string())),
                ))))
            ],
        })));

        assert_eq!(function("fun compare(a, b) {\n\treturn a.x == b.x\n}"), Ok(("", Function {
            name: "compare".to_string(),
            parameters: vec!["a".to_string(), "b".to_string()],
            body: vec![
                Declaration::Statement(Statement::Return(Some(Expression::Binary(
                    Box::new(Expression::Member(
                        Box::new(Expression::Identifier("a".to_string())), "x".to_string()
                    )),
                    Operation::Equal,
                    Box::new(Expression::Member(
                        Box::new(Expression::Identifier("b".to_string())), "x".to_string()
                    ))
                ))))
            ],
        })));
    }

    #[test]
    fn class_decl_test() {
        assert_eq!(class_declaration("class Baddie {\n\tfun add(x, y) {\n\t\treturn x + y\n\t}\n}"), Ok(("", Class {
            name: "Baddie".to_string(),
            base: None,
            methods: vec![Function {
                name: "add".to_string(),
                parameters: vec![
                    "x".to_string(),
                    "y".to_string(),
                ],
                body: vec![
                    Declaration::Statement(Statement::Return(Some(Expression::Binary(
                        Box::new(Expression::Identifier("x".to_string())),
                        Operation::Plus,
                        Box::new(Expression::Identifier("y".to_string())),
                    ))))
                ],
            }],
        })));
    }

    #[test]
    fn programs() {
        let program_a =
"\
class A {}

class B < A {
    fun add(a, b) {
        return a + b
    }
    fun compare(a, b) {
        return a.x == b.x
    }
}

fun my_printer() {
    print \"hello world\"
}

fun straight_line() {
    var a = 2
    var b = 3
    var c = a + b
    var d = a - c + \"text\"
}
";

        let program_a_expected = Ok(("", vec![
            Declaration::Class(Class {
                name: "A".to_string(),
                base: None,
                methods: vec![],
            }),

            Declaration::Class(Class {
                name: "B".to_string(),
                base: Some("A".to_string()),
                methods: vec![
                    Function {
                        name: "add".to_string(),
                        parameters: vec!["a".to_string(), "b".to_string()],
                        body: vec![
                            Declaration::Statement(Statement::Return(Some(Expression::Binary(
                                Box::new(Expression::Identifier("a".to_string())),
                                Operation::Plus,
                                Box::new(Expression::Identifier("b".to_string()))
                            ))))
                        ],
                    },

                    Function {
                        name: "compare".to_string(),
                        parameters: vec!["a".to_string(), "b".to_string()],
                        body: vec![
                            Declaration::Statement(Statement::Return(Some(Expression::Binary(
                                Box::new(Expression::Member(
                                    Box::new(Expression::Identifier("a".to_string())), "x".to_string()
                                )),
                                Operation::Equal,
                                Box::new(Expression::Member(
                                    Box::new(Expression::Identifier("b".to_string())), "x".to_string()
                                ))
                            ))))
                        ],
                    }
                ],
            }),

            Declaration::Function(Function {
                name: "my_printer".to_string(),
                parameters: vec![],
                body: vec![
                    Declaration::Statement(Statement::Print(Expression::Literal(Literal::String("hello world".to_string()))))
                ],
            }),

            Declaration::Function(Function {
                name: "straight_line".to_string(),
                parameters: vec![],
                body: vec![
                    Declaration::Variable(Variable { identifier: "a".to_string(), expression: Expression::Literal(Literal::Number(Number::Integer(2))) }),
                    Declaration::Variable(Variable { identifier: "b".to_string(), expression: Expression::Literal(Literal::Number(Number::Integer(3))) }),
                    Declaration::Variable(Variable { identifier: "c".to_string(), expression: Expression::Binary(
                        Box::new(Expression::Identifier("a".to_string())),
                        Operation::Plus,
                        Box::new(Expression::Identifier("b".to_string()))
                    ) }),
                    Declaration::Variable(Variable { identifier: "d".to_string(), expression: Expression::Binary(
                        Box::new(Expression::Identifier("a".to_string())),
                        Operation::Minus,
                        Box::new(Expression::Binary(
                            Box::new(Expression::Identifier("c".to_string())),
                            Operation::Plus,
                            Box::new(Expression::Literal(Literal::String("text".to_string())))
                        ))
                    ) })
                ],
            })
        ]));

        assert_eq!(program(program_a), program_a_expected);
    }
}

fn run(source: String) {
    let ast = program(source.as_str()).expect("parse failed").1;
    let mut printer = AstPrinter { printable: "".to_string() };
    ast.accept(&mut printer);
    println!("{}", printer.printable);
}

fn main() {
    let args: Vec<String> = env::args().collect();
    if args.len() < 2 {
        println!("usage: {} <script>", args[0]);
        return;
    }

    let source = fs::read_to_string(args[1].as_str()).expect("invalid filepath");

    run(source);
}
