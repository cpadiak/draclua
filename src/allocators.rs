use std::collections::HashMap;
use crate::bytecode::Operation;
use crate::bytecode_generator::GenerationAction;
use crate::runtime::{RegisterIndex, StringID};

pub enum RegisterUse {
    InUse,
    Free,
}

pub struct RegisterAllocation {
    pub register: RegisterIndex,
    pub prologue: Vec<Operation>,
    pub epilogue: Vec<Operation>,
}

pub struct RegisterAllocator {
    context: Vec<RegisterUse>,
    variables: HashMap<String, RegisterIndex>,
}

impl RegisterAllocator {
    pub fn has(&self, variable_name: &str) -> bool {
        self.variables.contains_key(variable_name)
    }

    pub fn allocate(&mut self, variable_name: &str) -> RegisterAllocation {
        match self.context.iter().position(|state| state == RegisterUse::Free) {
            Some(index) => Regi
        }
    }

    pub fn free(&mut self, variable_name: &str) {

    }
}

pub struct StringInterner {
    strings: HashMap<String, StringID>,
    next_id: StringID,
}

pub struct StringInternTable {

}

impl StringInterner {
    pub fn get_id(&mut self, s: String) -> StringID {
        if self.strings.contains_key(s.as_str()) {
            *self.strings.get(s.as_str()).unwrap()
        } else {
            let id = self.next_id;
            self.next_id += 1;
            self.strings.insert(s, id)
            id
        }
    }

    pub fn bake(&self) -> StringInternTable {

    }
}
