use std::collections::HashMap;

use crate::allocators::{RegisterAllocator, StringInterner};
use crate::bytecode::Operand;
use crate::bytecode::Operation as BytecodeOperation;
use crate::grammar::{Assignment, Block, Callsite, Class, Declaration, Expression, For, Function, Identifier, If, Literal, Number, Operation, Print, Program as GrammarProgram, Return, Variable, While};
use crate::runtime::{AnyID, Primitive, Program, Record, RecordID, RegisterIndex, Runtime, Stored, TableKey};
use crate::runtime::Program as BytecodeProgram;
use crate::visit::{Parsed, Visitor};

struct OperandAction {
    operand: Operand,
    prologue: Vec<BytecodeOperation>,
    epilogue: Vec<BytecodeOperation>,
}

impl OperandAction {
    fn new(operand: Operand) -> Self {
        OperandAction {
            operand,
            prologue: vec![],
            epilogue: vec![],
        }
    }
}

pub struct BytecodeGenerator {
    register_allocator: RegisterAllocator,
    string_interner: StringInterner,
    name_id_table: HashMap<String, AnyID>,
    runtime: Runtime,
    program: BytecodeProgram,
    current_record: RecordID,
}

impl BytecodeGenerator {
    fn resolve_variable(&mut self, variable_name: &str) -> OperandAction {
        if self.register_allocator.has(variable_name) {
            let ralloc = self.register_allocator.allocate(variable_name);
            let operand = Operand::Register(ralloc.register);
            OperandAction {
                operand,
                prologue: ralloc.prologue,
                epilogue: ralloc.epilogue,
            }
        } else if self.name_id_table.contains_key(variable_name) {
            let id = *self.name_id_table.get(variable_name).unwrap();
            self.resolve_id(id)
        } else if self.program.is_function(variable_name) {
            let operand = Operand::Literal(Primitive::Function(self.program.get_function(variable_name)));
            OperandAction::new(operand)
        } else {
            panic!("could not resolve {}", variable_name);
        }
    }

    fn resolve_id(&self, id: AnyID) -> OperandAction {
        let prim = match self.runtime.get(id) {
            Stored::Table(_) => Primitive::TableId(id),
            Stored::Record(_) => Primitive::RecordId(id),
        };
        OperandAction::new(Operand::Literal(prim))
    }

    fn resolve_member(&self, id: AnyID, member: &Primitive) -> OperandAction {
        let stored = self.runtime.get(id);
        let prim = match stored {
            Stored::Table(tbl) => {
                tbl.data.get(&TableKey::from_primitive(member)).unwrap().clone()
            }
            Stored::Record(rec) => {
                rec.get_field(member)
            }
        };
        OperandAction::new()
    }

    fn encode_literal(&mut self, literal: &Literal) -> Primitive {
        match literal {
            Literal::Number(num) => match num {
                Number::Integer(i) => Primitive::Integer(*i),
                Number::Float(f) => Primitive::Float(*f)
            }
            Literal::Boolean(b) => Primitive::Bool(*b),
            Literal::Nil => Primitive::Nil,
            Literal::String(s) => Primitive::String(self.string_interner.get_id(s.clone())),
        }
    }

    fn encode_expression(&mut self, expression: &Expression) -> OperandAction {
        match expression {
            Expression::Literal(lit) => OperandAction::new(Operand::Literal(self.encode_literal(lit))),
            Expression::This => self.resolve_id(self.current_record),
            Expression::Identifier(ident) => self.resolve_variable(ident),
            Expression::Member(exp, ident) => {
                let stored_id = self.encode_expression(exp);

            }
            Expression::Unary(_, _) => {}
            Expression::Binary(_, _, _) => {}
            Expression::CallArguments(_) => {}
            Expression::Group(_) => {}
        }
    }
}

impl Visitor for BytecodeGenerator {
    fn generate(&mut self, ast: &GrammarProgram) -> (Runtime, BytecodeProgram) {
        (self.runtime, self.program)
    }

    fn visit_program(&mut self, program: &GrammarProgram) {
        for decl in program {
            decl.accept(self);
        }
    }

    fn visit_class(&mut self, class: &Class) {
        let constructor = class.methods.iter()
            .find(|method| method.name == class.name)
            .expect("class {} does not have a constructor");

        let mut record = Record {
            name: class.name.to_string(),
            field_names: Default::default(),
            field_types: vec![],
            fields: vec![],
        };

        class.methods.iter().for_each(|method| {
            record.add_field(method.name.to_string(), Primitive::FunctionPointer.type_id());
            method.accept(self)
        });

        self.runtime.records.insert(class.name.to_string(), record);
    }

    fn visit_function(&mut self, function: &Function) {
        todo!()
    }

    fn visit_variable(&mut self, variable: &Variable) {
        let dst = self.resolve_variable(&variable.identifier);
        let src = self.encode_expression(&variable.expression);

        self.program.append(dst.prologue);
        self.program.append(src.prologue);

        self.program.push(BytecodeOperation::MOVE(dst.operand, src));

        self.program.append(src.epilogue);
        self.program.append(dst.epilogue);
    }

    fn visit_assignment(&mut self, statement: &Assignment) {}

    fn visit_for(&mut self, statement: &For) {
        todo!()
    }

    fn visit_if(&mut self, statement: &If) {
        todo!()
    }

    fn visit_print(&mut self, expression: &Print) {
        todo!()
    }

    fn visit_return(&mut self, expression: &Return) {
        todo!()
    }

    fn visit_while(&mut self, statement: &While) {
        todo!()
    }

    fn visit_block(&mut self, statement: &Block) {
        todo!()
    }

    fn visit_callsite(&mut self, statement: &Callsite) {
        todo!()
    }

    fn visit_integer(&mut self, literal: i64) {
        todo!()
    }

    fn visit_float(&mut self, literal: f64) {
        todo!()
    }

    fn visit_boolean(&mut self, literal: bool) {
        todo!()
    }

    fn visit_nil(&mut self) {
        todo!()
    }

    fn visit_string(&mut self, literal: &String) {
        todo!()
    }

    fn visit_this(&mut self) {
        todo!()
    }

    fn visit_identifier(&mut self, identifier: &Identifier) {
        todo!()
    }

    fn visit_member(&mut self, object: &Box<Expression>, member: &Identifier) {
        todo!()
    }

    fn visit_unary(&mut self, op: &Operation, exp: &Box<Expression>) {
        todo!()
    }

    fn visit_binary(&mut self, lexp: &Box<Expression>, op: &Operation, rexp: &Box<Expression>) {
        todo!()
    }

    fn visit_call_arguments(&mut self, call_arguments: &Vec<Expression>) {
        todo!()
    }

    fn visit_group(&mut self, group: &Box<Expression>) {
        todo!()
    }
}
