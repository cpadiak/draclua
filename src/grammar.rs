use std::any::{Any, TypeId};
use std::collections::HashMap;
use std::num::ParseIntError;

use nom::branch::alt;
use nom::bytes::complete::tag;
use nom::character::complete::{alpha0, char as nchar, digit0, multispace0, multispace1, one_of, satisfy, space0, space1};
use nom::character::complete::{digit1, none_of};
use nom::character::is_alphanumeric;
use nom::combinator::{eof, map, map_res, opt, recognize};
use nom::IResult;
use nom::multi::{many0, many1, many_till, separated_list0, separated_list1};
use nom::sequence::{delimited, pair, preceded, separated_pair, terminated, tuple};
use crate::runtime::Primitive;

/////////////////
// REPRESENTATION

#[derive(Debug, PartialEq, Copy, Clone)]
pub enum Number {
    Integer(i64),
    Float(f64),
}

#[derive(Debug, PartialEq, Clone)]
pub enum Literal {
    Number(Number),
    Boolean(bool),
    Nil,
    String(String),
}

pub type Identifier = String;

#[derive(Debug, PartialEq, Clone)]
pub enum Expression {
    Literal(Literal),
    This,
    Identifier(Identifier),
    Member(Box<Expression>, Identifier),
    Unary(Operation, Box<Expression>),
    Binary(Box<Expression>, Operation, Box<Expression>),
    CallArguments(Vec<Expression>),
    Group(Box<Expression>),
}

#[derive(Debug, PartialEq, Copy, Clone)]
pub enum Operation {
    LogicOr,
    LogicAnd,
    NotEqual,
    Equal,
    GreaterEqual,
    GreaterThan,
    LessEqual,
    LessThan,
    Plus,
    Minus,
    Divide,
    Multiply,
    Not,
    // Negative,
    Dot,
}

#[derive(Debug, PartialEq)]
pub enum Assignable {
    Identifier(Identifier),
    Callsite(Callsite),
}

#[derive(Debug, PartialEq)]
pub struct Assignment {
    pub assigned: Assignable,
    pub expression: Expression,
}

#[derive(Debug, PartialEq)]
pub struct Callsite {
    pub function: Expression,
    pub arguments: Vec<Expression>,
}

#[derive(Debug, PartialEq)]
pub enum Statement {
    Assignment(Assignment),
    For(For),
    If(If),
    Print(Print),
    Return(Return),
    While(While),
    Block(Block),
    Callsite(Callsite),
}

#[derive(Debug, PartialEq)]
pub struct For {
    pub init_variable: Option<Variable>,
    pub condition: Option<Expression>,
    pub iter_variable: Option<Box<Statement>>,
    pub body: Block,
}

#[derive(Debug, PartialEq)]
pub struct If {
    pub condition: Expression,
    pub body: Block,
    pub else_block: Option<Block>,
}

pub type Print = Expression;
pub type Return = Option<Expression>;

#[derive(Debug, PartialEq)]
pub struct While {
    pub condition: Expression,
    pub body: Block,
}

pub type Block = Vec<Declaration>;

#[derive(Debug, PartialEq)]
pub enum Declaration {
    Class(Class),
    Function(Function),
    Variable(Variable),
    Member(MemberDeclaration),
    Statement(Statement),
}

#[derive(Debug, PartialEq)]
pub struct Class {
    pub name: Identifier,
    pub base: Option<Identifier>,
    pub members: HashMap<String, TypeId>,
    pub methods: Vec<Function>,
}

#[derive(Debug, PartialEq)]
pub struct Function {
    pub name: Identifier,
    pub parameters: Vec<Identifier>,
    pub body: Block,
}

#[derive(Debug, PartialEq)]
pub struct MemberDeclaration {
    pub name: Identifier,
    pub type_identifier: TypeId,
}

#[derive(Debug, PartialEq)]
pub struct Variable {
    pub identifier: Identifier,
    pub expression: Expression,
}

pub type Program = Vec<Declaration>;

//////////
// LEXICAL

fn positive_to_number(text: &str) -> Result<Number, ParseIntError> {
    text.parse().map(Number::Integer)
}

fn negative_to_number(text: &str) -> Number {
    Number::Integer(-(text.parse::<i64>().unwrap()))
}

pub fn integer(source: &str) -> IResult<&str, Number> {
    let positive_integer = map_res(digit1, positive_to_number);
    let negative_integer = map(preceded(tag("-"), digit1), negative_to_number);
    alt((positive_integer, negative_integer))(source)
}

pub fn float(source: &str) -> IResult<&str, Number> {
    let convert_to_float = |float_str: &str| Number::Float(float_str.parse::<f64>().unwrap());
    map(recognize(pair(alt((tag(""), tag("+"), tag("-"))), separated_pair(digit1, nchar('.'), digit0))), convert_to_float)(source)
}

pub fn number(source: &str) -> IResult<&str, Number> {
    alt((float, integer))(source)
}

pub fn string(source: &str) -> IResult<&str, String> {
    map(
        delimited(nchar('\"'), many0(none_of("\"")), nchar('\"')),
        |str_vec: Vec<char>| str_vec.iter().collect(),
    )(source)
}

fn alpha(text: &str) -> IResult<&str, char> {
    satisfy(|c| ('a' <= c.to_ascii_lowercase() && c.to_ascii_lowercase() <= 'z') || c == '_')(text)
}

pub fn boolean(source: &str) -> IResult<&str, Literal> {
    alt((
        map(tag("true"), |b_str: &str| Literal::Boolean(b_str.parse::<bool>().unwrap())),
        map(tag("false"), |b_str: &str| Literal::Boolean(b_str.parse::<bool>().unwrap())),
    ))(source)
}

//////////////
// EXPRESSIONS

pub fn atomic_expression(source: &str) -> IResult<&str, Expression> {
    alt((
        map(boolean, Expression::Literal),
        map(tag("nil"), |_nil_str: &str| Expression::Literal(Literal::Nil)),
        map(tag("this"), |_this_str: &str| Expression::This),
        map(number, |num| Expression::Literal(Literal::Number(num))),
        map(string, |s_str: String| Expression::Literal(Literal::String(s_str))),
        map(identifier, Expression::Identifier),
        map(arguments, Expression::CallArguments),
        map(delimited(nchar('('), expression, nchar(')')), |exp| Expression::Group(Box::new(exp))),
        map(pair(unary, expression), |exp| Expression::Unary(exp.0, Box::new(exp.1))),
    ))(source)
}

pub fn expression(source: &str) -> IResult<&str, Expression> {
    fn make_binary_expression(first: (Expression, Operation, Expression), chain: Vec<(Operation, Expression)>) -> Expression {
        let mut result = Expression::Binary(Box::new(first.0), first.1, Box::new(first.2));
        for (op, exp) in chain {
            result = Expression::Binary(Box::new(result), op, Box::new(exp));
        }
        result
    }

    fn non_recursive(source: &str) -> IResult<&str, Expression> {
        let property = map(
            separated_pair(atomic_expression, nchar('.'), identifier),
            |(object, member)| Expression::Member(Box::new(object), member.to_string()),
        );

        alt((property, atomic_expression))(source)
    }

    let recursive_expression = map(
        pair(tuple((non_recursive, delimited(space1, binary, space1), expression)),
             many0(pair(binary, expression))),
        |result| make_binary_expression(result.0, result.1),
    );

    alt((
        recursive_expression,
        non_recursive,
    ))(source)
}

pub fn identifier(source: &str) -> IResult<&str, Identifier> {
    let valid_chars = |c: char| is_alphanumeric(u8::try_from(c).unwrap()) || c == '_';
    map(recognize(pair(alpha, many0(satisfy(valid_chars)))), |i_str| i_str.to_string())(source)
}

pub fn type_identifier(source: &str) -> IResult<&str, TypeId> {
    Primitive::Bool.type_id()
}

pub fn unary(source: &str) -> IResult<&str, Operation> {
    alt((
        map(tag("!"), |_op| Operation::Not),
        map(tag("-"), |_op| Operation::Minus),
        map(tag("+"), |_op| Operation::Plus),
    ))(source)
}

pub fn binary(source: &str) -> IResult<&str, Operation> {
    alt((
        map(tag("or"), |_op| Operation::LogicOr),
        map(tag("and"), |_op| Operation::LogicAnd),
        map(tag("!="), |_op| Operation::NotEqual),
        map(tag("=="), |_op| Operation::Equal),
        map(tag(">="), |_op| Operation::GreaterEqual),
        map(tag(">"), |_op| Operation::GreaterThan),
        map(tag("<="), |_op| Operation::LessEqual),
        map(tag("<"), |_op| Operation::LessThan),
        map(tag("-"), |_op| Operation::Minus),
        map(tag("+"), |_op| Operation::Plus),
        map(tag("/"), |_op| Operation::Divide),
        map(tag("*"), |_op| Operation::Multiply),
    ))(source)
}

pub fn callsite(source: &str) -> IResult<&str, Callsite> {
    let method = tuple((nchar('.'), identifier));
    map(
        tuple((
            expression,
            many1(alt((
                map(arguments, Expression::CallArguments),
                map(method, |identifier| Expression::Group(Box::new(Expression::Identifier(identifier.1.to_string())))),
            )))
        )),
        |site: (Expression, Vec<Expression>)| Callsite { function: site.0, arguments: site.1 },
    )(source)
}

//////////
// UTILITY

fn parenthesized<R>(element: fn(&str) -> IResult<&str, R>) -> impl Fn(&str) -> IResult<&str, Vec<R>> {
    move |source: &str| delimited(
        nchar('('),
        separated_list0(tuple((space0, nchar(','), space0)), element),
        nchar(')'),
    )(source)
}

fn spaced_list0<R>(element: fn(&str) -> IResult<&str, R>) -> impl Fn(&str) -> IResult<&str, Vec<R>> {
    move |source: &str| delimited(multispace0, separated_list0(multispace1, element), multispace0)(source)
}

fn block_of<R>(element: fn(&str) -> IResult<&str, R>) -> impl Fn(&str) -> IResult<&str, Vec<R>> {
    move |source: &str| delimited(nchar('{'), spaced_list0(element), nchar('}'))(source)
}

/////////////
// STATEMENTS

pub fn assignment(source: &str) -> IResult<&str, Assignment> {
    let assignable = alt((
        map(callsite, Assignable::Callsite),
        map(identifier, Assignable::Identifier),
    ));
    map(
        separated_pair(assignable, tuple((space0, nchar('='), space0)), expression),
        |(assigned, expression)| Assignment { assigned, expression },
    )(source)
}

pub fn block(source: &str) -> IResult<&str, Block> {
    block_of(declaration)(source)
}

pub fn while_statement(source: &str) -> IResult<&str, While> {
    map(
        preceded(
            tag("while"),
            pair(
                delimited(nchar('('), expression, nchar(')')),
                block)),
        |whl| While { condition: whl.0, body: whl.1 },
    )(source)
}

pub fn return_statement(source: &str) -> IResult<&str, Return> {
    preceded(pair(tag("return"), space0), opt(expression))(source)
}

pub fn print_statement(source: &str) -> IResult<&str, Print> {
    preceded(pair(tag("print"), space1), expression)(source)
}

pub fn if_statement(source: &str) -> IResult<&str, If> {
    map(
        preceded(
            tag("if"),
            tuple((
                delimited(pair(space1, nchar('(')), expression, pair(nchar(')'), space1)),
                block,
                opt(preceded(delimited(space0, tag("else"), space0), block))
            )),
        ),
        |ifelse: (Expression, Block, Option<Block>)| If { condition: ifelse.0, body: ifelse.1, else_block: ifelse.2 },
    )(source)
}

pub fn for_statement(source: &str) -> IResult<&str, For> {
    let init = terminated(delimited(space0, opt(variable_declaration), space0), nchar(';'));
    let cond = terminated(delimited(space0, opt(expression), space0), nchar(';'));
    let iter = delimited(space0, opt(map(statement, Box::new)), space0);
    let loop_setup = delimited(pair(space0, nchar('(')), tuple((init, cond, iter)), pair(nchar(')'), space0));
    map(
        preceded(tag("for"), pair(loop_setup, block)),
        |((init_variable, condition, iter_variable), body)|
            For { init_variable, condition, iter_variable, body },
    )(source)
}

pub fn statement(source: &str) -> IResult<&str, Statement> {
    alt((
        map(for_statement, Statement::For),
        map(if_statement, Statement::If),
        map(print_statement, Statement::Print),
        map(return_statement, Statement::Return),
        map(while_statement, Statement::While),
        map(assignment, Statement::Assignment),
        map(callsite, Statement::Callsite),
        map(block, Statement::Block),
    ))(source)
}

///////////////
// DECLARATIONS

pub fn variable_declaration(source: &str) -> IResult<&str, Variable> {
    map(
        preceded(
            pair(tag("var"), space1),
            separated_pair(identifier, tuple((space0, nchar('='), space0)), expression),
        ),
        |decl| Variable {
            identifier: decl.0.to_string(),
            expression: decl.1,
        },
    )(source)
}

pub fn member_declaration(source: &str) -> IResult<&str, MemberDeclaration> {
    map(
        separated_pair(identifier, pair(nchar(':'), space0), type_identifier),
        |(name, type_identifier)| MemberDeclaration { name, type_identifier }
    )(source)
}

pub fn arguments(source: &str) -> IResult<&str, Vec<Expression>> {
    parenthesized(expression)(source)
}

pub fn parameters(source: &str) -> IResult<&str, Vec<Identifier>> {
    parenthesized(identifier)(source)
}

pub fn function(source: &str) -> IResult<&str, Function> {
    map(
        preceded(pair(tag("fun"), space1),
                 tuple((
                     terminated(identifier, space0),
                     terminated(parameters, space0),
                     block
                 ))),
        |def| Function {
            name: def.0.to_string(),
            parameters: def.1,
            body: def.2,
        },
    )(source)
}

pub fn class_declaration(source: &str) -> IResult<&str, Class> {
    let inheritance = map(
        preceded(pair(nchar('<'), space0), identifier),
        |base| base.to_string(),
    );
    let spaced_inheritance = opt(terminated(inheritance, space0));
    map(
        preceded(
            pair(tag("class"), space1),
            tuple((terminated(identifier, space0), spaced_inheritance, block_of(), block_of(function))),
        ),
        |class: (String, Option<String>, Vec<Function>)| Class {
            name: class.0,
            base: class.1,
            members: Default::default(),
            methods: class.2,
        },
    )(source)
}

pub fn declaration(source: &str) -> IResult<&str, Declaration> {
    alt((
        map(class_declaration, Declaration::Class),
        map(function, Declaration::Function),
        map(variable_declaration, Declaration::Variable),
        map(statement, Declaration::Statement),
    ))(source)
}

//////////
// PROGRAM

pub fn program(source: &str) -> IResult<&str, Program> {
    delimited(
        multispace0,
        separated_list1(multispace0, declaration),
        multispace0,
    )(source)
}
