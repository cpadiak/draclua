use crate::runtime::{Primitive, RegisterIndex};

pub enum Operation {
    MOVE(Operand, Operand),
}

pub enum Operand {
    Literal(Primitive),
    Register(RegisterIndex),
    Memory(usize),
}
