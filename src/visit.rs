use crate::grammar::{Assignable, Assignment, Block, Callsite, Class, Declaration, Expression, For, Function, Identifier, If, Literal, Number, Operation, Print, Program, Return, Statement, Variable, While};

pub trait Visitor {
    fn visit_program(&mut self, program: &Program);
    fn visit_class(&mut self, class: &Class);
    fn visit_function(&mut self, function: &Function);
    fn visit_variable(&mut self, variable: &Variable);

    fn visit_assignment(&mut self, statement: &Assignment);
    fn visit_for(&mut self, statement: &For);
    fn visit_if(&mut self, statement: &If);
    fn visit_print(&mut self, expression: &Print);
    fn visit_return(&mut self, expression: &Return);
    fn visit_while(&mut self, statement: &While);
    fn visit_block(&mut self, statement: &Block);
    fn visit_callsite(&mut self, statement: &Callsite);

    fn visit_integer(&mut self, literal: i64);
    fn visit_float(&mut self, literal: f64);
    fn visit_boolean(&mut self, literal: bool);
    fn visit_nil(&mut self);
    fn visit_string(&mut self, literal: &String);
    fn visit_this(&mut self);
    fn visit_identifier(&mut self, identifier: &Identifier);
    fn visit_member(&mut self, object: &Box<Expression>, member: &Identifier);
    fn visit_unary(&mut self, op: &Operation, exp: &Box<Expression>);
    fn visit_binary(&mut self, lexp: &Box<Expression>, op: &Operation, rexp: &Box<Expression>);
    fn visit_call_arguments(&mut self, call_arguments: &Vec<Expression>);
    fn visit_group(&mut self, group: &Box<Expression>);
}

pub trait Parsed {
    fn accept<V: Visitor>(&self, visitor: &mut V);
}

impl Parsed for Program {
    fn accept<V: Visitor>(&self, visitor: &mut V) {
        visitor.visit_program(self)
    }
}

impl Parsed for Declaration {
    fn accept<V: Visitor>(&self, visitor: &mut V) {
        match self {
            Declaration::Class(decl) => { visitor.visit_class(decl) }
            Declaration::Function(decl) => { visitor.visit_function(decl) }
            Declaration::Variable(decl) => { visitor.visit_variable(decl) }
            Declaration::Statement(decl) => { decl.accept(visitor) }
        }
    }
}

impl Parsed for Class {
    fn accept<V: Visitor>(&self, visitor: &mut V) {
        visitor.visit_class(self)
    }
}

impl Parsed for Function {
    fn accept<V: Visitor>(&self, visitor: &mut V) {
        visitor.visit_function(self)
    }
}

impl Parsed for Variable {
    fn accept<V: Visitor>(&self, visitor: &mut V) {
        visitor.visit_variable(self)
    }
}

impl Parsed for Assignable {
    fn accept<V: Visitor>(&self, visitor: &mut V) {
        match self {
            Assignable::Identifier(assignable) => { visitor.visit_identifier(assignable) }
            Assignable::Callsite(assignable) => { visitor.visit_callsite(assignable) }
        }
    }
}

impl Parsed for Statement {
    fn accept<V: Visitor>(&self, visitor: &mut V) {
        match self {
            Statement::Assignment(stmt) => { visitor.visit_assignment(stmt) }
            Statement::For(stmt) => { visitor.visit_for(stmt) }
            Statement::If(stmt) => { visitor.visit_if(stmt) }
            Statement::Print(stmt) => { visitor.visit_print(stmt) }
            Statement::Return(stmt) => { visitor.visit_return(stmt) }
            Statement::While(stmt) => { visitor.visit_while(stmt) }
            Statement::Block(stmt) => { visitor.visit_block(stmt) }
            Statement::Callsite(stmt) => { visitor.visit_callsite(stmt) }
        }
    }
}

impl Parsed for Expression {
    fn accept<V: Visitor>(&self, visitor: &mut V) {
        match self {
            Expression::Literal(exp) => { exp.accept(visitor) }
            Expression::This => { visitor.visit_this() }
            Expression::Identifier(exp) => { visitor.visit_identifier(exp) }
            Expression::Member(object, member) => { visitor.visit_member(object, member) }
            Expression::Unary(op, exp) => { visitor.visit_unary(op, exp) }
            Expression::Binary(lexp, op, rexp) => { visitor.visit_binary(lexp, op, rexp) }
            Expression::CallArguments(exps) => { visitor.visit_call_arguments(exps) }
            Expression::Group(exp) => { visitor.visit_group(exp) }
        }
    }
}

impl Parsed for Literal {
    fn accept<V: Visitor>(&self, visitor: &mut V) {
        match self {
            Literal::Number(lit) => {
                match lit {
                    Number::Integer(num) => { visitor.visit_integer(*num) }
                    Number::Float(num) => { visitor.visit_float(*num) }
                }
            }
            Literal::Boolean(lit) => { visitor.visit_boolean(*lit) }
            Literal::Nil => { visitor.visit_nil() }
            Literal::String(lit) => { visitor.visit_string(lit) }
        }
    }
}
